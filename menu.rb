#!/usr/bin/env ruby

require 'nokogiri'
require 'open-uri'
require 'json'
require 'tilt'

class Restaurant
	attr_accessor :name
	attr_accessor :menus
	def initialize(the_name)
		self.name = the_name
		self.menus = []
	end

end

class Menu
	attr_accessor :items
	attr_accessor :name
	def initialize(the_name)
		self.name = the_name
		self.items = []
	end
end

date = Date.today
$query = ARGV[0]

def query_has(r)
	!$query || $query =~ r
end

restaurants = []


if query_has /rea[ck]tori/i

reaktori = JSON.parse(open("https://www.fazerfoodco.fi/api/restaurant/menu/week?language=en&restaurantPageId=180077&weekDate=#{date.strftime("%Y-%-m-%-d")}").read)
restaurants << Restaurant.new("Reaktori")
reaktori["LunchMenus"].each do |dw|
	next unless dw["Date"] == date.strftime("%-d.%-m.%Y")
	dw["SetMenus"].each do |sm|
		restaurants.last.menus << Menu.new(sm["Name"])
		sm["Meals"].each do |meal|
			restaurants.last.menus.last.items << "#{meal["Name"]} [#{meal["Diets"].join(",")}]"
		end
	end
end

end

if query_has /hertsi|tietotalo/i

restaurants << Restaurant.new("Hertsi")
hertsi = Nokogiri.parse(open("https://www.sodexo.fi/en/node/111"))

weekday_tab = hertsi.at("div[@id='tabs-#{date.wday-1}']")
weekday_tab.search("div.mealrow").each do |mealrow|
	
	restaurants.last.menus << Menu.new(mealrow.at("span.meal-type").inner_text)
	restaurants.last.menus.last.items << mealrow.at("p.meal-name").inner_text
end

end

# 6 60 41 5

# find out weekday shit
d = Nokogiri.parse(open("http://www.juvenes.fi/konehuone"))
week, weekday = nil, nil
d.search("div.LunchMenu").each do |div|
	if div["week"] && div["weekday"]
		week, weekday = div["week"], div["weekday"]
	end
end

def scrape_juvenes(kid, mid, week, weekday)
	JSON.parse(JSON.parse(open("https://www.juvenes.fi/DesktopModules/Talents.LunchMenu/LunchMenuServices.asmx/GetMenuByWeekday?KitchenId=#{kid}&MenuTypeId=#{mid}&Week=#{week}&Weekday=#{weekday}&lang=%27en%27&format=json&callback=angular.callbacks._4").read.scan(/angular.callbacks._4\((.*)\);$/)[0][0])["d"])["MealOptions"]
end

if query_has /newton/i

restaurants << Restaurant.new("Newton")
newton = scrape_juvenes(6, 60, week, weekday) + scrape_juvenes(6, 86, week, weekday)
newton.each do |mo|
	restaurants.last.menus << Menu.new(mo["Name_EN"])
	mo["MenuItems"].each do |mi|
		next unless mi["DisplayStyle"] == 0
		restaurants.last.menus.last.items << "#{mi["Name_EN"]} [#{mi["Diets"]}]"
	end
end

end

if query_has /kone/i

restaurants << Restaurant.new("Konehuone")
kh = scrape_juvenes(60038, 77, week, weekday) + scrape_juvenes(60038, 3, week, weekday) + scrape_juvenes(60038, 0, week, weekday)
kh.each do |mo|
	restaurants.last.menus << Menu.new(mo["Name_EN"])
	mo["MenuItems"].each do |mi|
		next unless mi["DisplayStyle"] < 2
		restaurants.last.menus.last.items << "#{mi["Name_EN"]} [#{mi["Diets"]}]"
	end
end

end
tmpl = Tilt::ERBTemplate.new {File.read("menu.html.erb")}
puts tmpl.render(nil, restaurants: restaurants, date: date)
